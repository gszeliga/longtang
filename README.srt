Welcome to Longtang
===================

What's Longtang?
----------------

Longtang is a command-line software based on `actors model <https://en.wikipedia.org/wiki/Actor_model>`_ capable of processing any sloppy mp3 collection that you have into a well-organized, easy-to-locate tagged media library.

Why should I use it?
--------------------

Because it will save you a **HUGE** amount of time by:

* Tagging all your *mp3* files with the right ID3Tag information.
* Downloading album cover art **(New feature!!)**
* Decompressing and tagging any **zip**, **rar** or **7z** file that you have with music on it transparently.
* Creating a hierarchical media tree organized by artists and albums.
* Naming all files in a simple and yet clear syntax.
* Detecting tag issues that prevent files from being accurately classified.
* Avoiding any stranded or unknown music files inside any portable music player (which usually happens in Creative Zen players, just to name an example) which take up storage space and are not easily detected.

Installation guide:
-------------------

Before proceeding with the installation, please check if your system already has the following packages installed:


* Python 2.7.x and headers (python-dev)
* Chromaprint tools (libchromaprint-tools)
* Libevent headers (libevent-dev)
* Libxml (libxml2-dev and libxslt-dev)
* unrar and 7z commands available within your system _PATH_ 


After that, you can proceed to install in either two manners: from the source code or from `Pypi <https://pypi.python.org/pypi>`_ by using `pip <http://www.pip-installer.org/en/latest/>`_ utility.

1. Using the source code
~~~~~~~~~~~~~~~~~~~~~~~~

1. Checkout the source code::


    git clone https://bitbucket.org/gszeliga/longtang.git


2. Execute the setup utility as follows::

    python setup.py install


2. Using *pip*
~~~~~~~~~~~~~~~~

Just type on your command line::

    pip install longtang


Command guide:
--------------

The main binary is called *longtang* (as you can imagine) and it accepts the following parameters:

* --source  Source path where all the to-be-processed files are located.
* --target  Target path where the hierarchical structure will be created.
* --verbosity  Level of debug information to be printed. Accepted values are: *DEBUG*, *INFO*, *WARN* or *ERROR*. Default value is *NONE*
* --override-tags  (Optional) Whether you want *Longtang* to override any id3tag information on the source music files. *Bear in mind that if the amount of files is high it will turn the process to be real slow*.
* --offline  (Optional) Since any missing id3tag information will be retrieved using `Acoustid <http://acoustid.org/chromaprint>`_ service, maybe you might be interesed in not performing this action and just handle your media collection with the already existing id3 information and point out any failure during the process.
* --disable-cover-art: (Optional) Disables resolution of all cover art from all matched artists and albums.
* --help  Prints out help information


What's coming next?
-------------------

Well, i currently have a lot of ideas but, in a short term, i'm thinking in:

* Supporting more music formats: ogg, mpc, flac and so on.
* Porting source code into Python 3.
* Replacing _gevent_ with _multiprocessing_ library so that we gain execution _**parallelism**_ (take a look at branch **features/multiprocessing** for further details).

Bugs and Feedback
-----------------

You can contact me at *gszeliga@gmail.com* or follow me on twitter: `@gszeliga <https://twitter.com/gszeliga>`_
