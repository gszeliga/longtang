#https://github.com/hamcrest/PyHamcrest
#http://packages.python.org/PyHamcrest/

import gevent
import unittest
import os

from hamcrest import *
from longtang.actors.supervisors.filepackaging import filepackaging, messages, configuration, factory
from longtang.system import system
from longtang.actors import actorstestutils
from longtang.common import domain as common_domain
from longtang.actors.tracking import tracking, facade

class TestFilePackagingSupervisor(unittest.TestCase):

	def setUp(cls):
		global base_test_dir
		base_test_dir = actorstestutils.create_tmp_dir(suffix='filepackaging_test')

	def tearDown(cls):
		actorstestutils.remove_tmp_dir(base_test_dir)

	def test_creation(self):

		config = configuration.FilePackagingConfigurationBuilder().offline_mode(False).build()

		actor_system = system.ActorSystem()
		filepackaging_actor = actor_system.with_factory(factory.FilePackagingSupervisorFactory(config),'file-packaging-supervisor')

		try:
			assert_that(actor_system.find_by_id('file-packaging-supervisor'), is_not(None), 'File packaging supervisor does not exist within system')
			assert_that(actor_system.find_by_id('id3-folder-manager-actor'), is_not(None), 'Id3 folder manager actor does not exist within system')
			assert_that(actor_system.find_by_id('id3-file-manager-actor'), is_not(None), 'Id3 file manager actor does not exist within system')
			assert_that(actor_system.find_by_id('id3-file-tagger-actor'), is_not(None), 'Id3 file tagger actor does not exist within system')		

			assert_that(actor_system.find_by_id('id3-folder-manager-actor').parent(), is_(equal_to(actor_system.find_by_id('file-packaging-supervisor'))),'Parent actor is not correctly assigned')
			assert_that(actor_system.find_by_id('id3-file-manager-actor').parent(), is_(equal_to(actor_system.find_by_id('file-packaging-supervisor'))),'Parent actor is not correctly assigned')
			assert_that(actor_system.find_by_id('id3-file-tagger-actor').parent(), is_(equal_to(actor_system.find_by_id('file-packaging-supervisor'))),'Parent actor is not correctly assigned')
			
		except exceptions.ActorNotFound as e:
			self.fail(str(e))
						
		actor_system.shutdown()

	def test_file_packaging(self):

		config = configuration.FilePackagingConfigurationBuilder().offline_mode(False).cover_art_enabled(False).build()

		data_dir = os.path.join(actorstestutils.current_dirpath(__file__), 'data')
		source_file = os.path.join(data_dir, 'with_id3taginfo.mp3')

		metadata_builder = common_domain.FileMetadataBuilder()
		metadata = metadata_builder.artist('My Test Artist').title('My Test Title').album('My Test Album').track_number('7').build()
		
		filepackaging_actor = actorstestutils.TestActorBuilder().with_factory(factory.FilePackagingSupervisorFactory(config))\
															.with_id('test-filepackaging')\
															.termination_moratorium(10)\
															.terminate_system(False)\
															.termination_type(messages.FilePackagingFinished).build()

		tracking_facade = facade.MediaTrackingFacade.create_within(filepackaging_actor.system())
		tracking_id = tracking_facade.create_audio_tracking(source_file)

		filepackaging_actor.tell(messages.PerformFilePackaging(source_file, metadata, base_test_dir, tracking_id))

		assert_that(filepackaging_actor.inspector().num_instances(messages.FilePackagingFinished), is_(equal_to(1)), 'Total amount of FilePackagingFinished messages received is wrong')

		full_dir_path= os.path.join(base_test_dir, 'My Test Artist', 'My Test Album')
		full_file_path=os.path.join(full_dir_path,'07 - My Test Title.mp3')

		assert_that(os.path.isdir(full_dir_path), is_(equal_to(True)), 'Artist or album directory does not exist or full name is not right')
		assert_that(os.path.isfile(full_file_path), is_(equal_to(True)),'Packaged file does not exist')

		message = filepackaging_actor.inspector().first(messages.FilePackagingFinished)

		#Tracking information
		assert_that(message.tracking(), is_(equal_to(tracking_id)), 'Tracking id does not match')

		tracking_info = tracking_facade.lookup(tracking_id)

		assert_that(tracking_info.tracking_events.of_type('FILE_PACKAGING_DONE').is_empty(), is_(False),'Tracking events of type FILE_PACKAGING_DONE is empty')
		assert_that(tracking_info.mediafile.destinationpath, is_(not_none()), 'Destination path of mediafile is empty')
		assert_that(tracking_info.mediafile.destinationpath, is_(equal_to(full_file_path)), 'Destination path value is not as expected')

	def test_file_packaging_copy_failure(self):

		config = configuration.FilePackagingConfigurationBuilder().offline_mode(False).cover_art_enabled(False).build()

		data_dir = os.path.join(actorstestutils.current_dirpath(__file__), 'data')
		source_file = os.path.join(data_dir, 'nonexisting_id3taginfo.mp3')

		metadata_builder = common_domain.FileMetadataBuilder()
		metadata = metadata_builder.artist('My Test Artist').title('My Test Title').album('My Test Album').track_number('7').build()
		
		filepackaging_actor = actorstestutils.TestActorBuilder().with_factory(factory.FilePackagingSupervisorFactory(config))\
															.with_id('test-filepackaging-failure')\
															.termination_moratorium(10)\
															.terminate_system(False)\
															.termination_type(messages.FilePackagingFailure).build()

		tracking_facade = facade.MediaTrackingFacade.create_within(filepackaging_actor.system())
		tracking_id = tracking_facade.create_audio_tracking(source_file)

		filepackaging_actor.tell(messages.PerformFilePackaging(source_file, metadata, base_test_dir, tracking_id))

		assert_that(filepackaging_actor.inspector().num_instances(messages.FilePackagingFailure), is_(equal_to(1)), 'Total amount of FilePackagingFailure messages received is wrong')

		message = filepackaging_actor.inspector().first(messages.FilePackagingFailure)

		#Tracking information
		assert_that(message.tracking(), is_(equal_to(tracking_id)), 'Tracking id does not match')

		tracking_info = tracking_facade.lookup(tracking_id)

		assert_that(tracking_info.tracking_events.of_type('FILE_COPY_FAILURE').is_empty(), is_(False),'Tracking events of type FILE_COPY_FAILED is empty')

	def test_file_packaging_folder_creation_failure(self):

		config = configuration.FilePackagingConfigurationBuilder().offline_mode(False).cover_art_enabled(False).build()


		data_dir = os.path.join(actorstestutils.current_dirpath(__file__), 'data')
		source_file = os.path.join(data_dir, 'with_id3taginfo.mp3')

		metadata_builder = common_domain.FileMetadataBuilder()
		metadata = metadata_builder.artist('My Test Artist').title('My Test Title').album('My Test Album').track_number('7').build()
		
		filepackaging_actor = actorstestutils.TestActorBuilder().with_factory(factory.FilePackagingSupervisorFactory(config))\
															.with_id('test-filepackaging-failure')\
															.termination_moratorium(10)\
															.terminate_system(False)\
															.termination_type(messages.FilePackagingFailure).build()

		tracking_facade = facade.MediaTrackingFacade.create_within(filepackaging_actor.system())
		tracking_id = tracking_facade.create_audio_tracking(source_file)

		filepackaging_actor.tell(messages.PerformFilePackaging(source_file, metadata, '/', tracking_id))

		assert_that(filepackaging_actor.inspector().num_instances(messages.FilePackagingFailure), is_(equal_to(1)), 'Total amount of FilePackagingFailure messages received is wrong')

		message = filepackaging_actor.inspector().first(messages.FilePackagingFailure)

		#Tracking information
		assert_that(message.tracking(), is_(equal_to(tracking_id)), 'Tracking id does not match')

		tracking_info = tracking_facade.lookup(tracking_id)

		assert_that(tracking_info.tracking_events.of_type('FOLDER_CREATION_FAILURE').is_empty(), is_(False),'Tracking events of type FOLDER_CREATION_FAILED is empty')

	def test_file_packaging_metadata_writting_failure(self):

		config = configuration.FilePackagingConfigurationBuilder().offline_mode(False).cover_art_enabled(False).build()

		data_dir = os.path.join(actorstestutils.current_dirpath(__file__), 'data')
		source_file = os.path.join(data_dir, 'with_id3taginfo.mp3')

		metadata_builder = common_domain.FileMetadataBuilder()
		metadata = metadata_builder.artist('My Test Artist').title('My Test Title').album(None).track_number('7').build()
		
		filepackaging_actor = actorstestutils.TestActorBuilder().with_factory(factory.FilePackagingSupervisorFactory(config))\
															.with_id('test-filepackaging-failure')\
															.termination_moratorium(10)\
															.terminate_system(False)\
															.termination_type(messages.FilePackagingFailure).build()

		tracking_facade = facade.MediaTrackingFacade.create_within(filepackaging_actor.system())
		tracking_id = tracking_facade.create_audio_tracking(source_file)

		filepackaging_actor.tell(messages.PerformFilePackaging(source_file, metadata, '/', tracking_id))

		assert_that(filepackaging_actor.inspector().num_instances(messages.FilePackagingFailure), is_(equal_to(1)), 'Total amount of FilePackagingFailure messages received is wrong')

		message = filepackaging_actor.inspector().first(messages.FilePackagingFailure)

		#Tracking information
		assert_that(message.tracking(), is_(equal_to(tracking_id)), 'Tracking id does not match')

		tracking_info = tracking_facade.lookup(tracking_id)

		assert_that(tracking_info.tracking_events.of_type('FOLDER_CREATION_FAILURE').is_empty(), is_(False),'Tracking events of type FOLDER_CREATION_FAILED is empty')	

if __name__ == '__main__':
	unittest.main()   